﻿using System;
using System.Net.Http.Headers;

namespace Dont_Froget_Me
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHeader();

            MilitaryFrog militaryFrogObject = new MilitaryFrog
            {
                Name = "Disarray",
                Weight = 223.5,
                Rank = "General",
                Weapon = new Weapon("Rifle", 7, 12)
            };

            militaryFrogObject.Salute();
            militaryFrogObject.Climb();
            militaryFrogObject.DigTrench();
            militaryFrogObject.Eat();
            militaryFrogObject.Hop(100);
            MountainSearchAndRescue(militaryFrogObject);

            Degree phd = new Degree()
            {
                YearAwarded = 2016,
                Specialization = "Nano Meta-Philosphical Physics",
                Institution = "Moscow University"
            };

            ScientistFrog scientistFrogObject = new ScientistFrog
            {
                Name = "Petri",
                Weight = 122,
                Subject = "Physics",
                Degree = phd
            };

            scientistFrogObject.Eat();

            if (scientistFrogObject.IsValidTestSubject(militaryFrogObject) == false)
                Console.WriteLine($"{militaryFrogObject.Rank} {militaryFrogObject.Name} is too buff for the serum");
            else
                scientistFrogObject.Experiment();
        }

        public static void MountainSearchAndRescue(IClimb saviour)
        {
            Console.WriteLine("We found a climber to help");
            saviour.Climb();
            Console.WriteLine("It is going to be ok!!");
        }

        public static void PrintHeader()
        {
            string header = @"


______            _ _    ______                    _    ___  ___     
|  _  \          ( ) |   |  ___|                  | |   |  \/  |     
| | | |___  _ __ |/| |_  | |_ _ __ ___   __ _  ___| |_  | .  . | ___ 
| | | / _ \| '_ \  | __| |  _| '__/ _ \ / _` |/ _ \ __| | |\/| |/ _ \
| |/ / (_) | | | | | |_  | | | | | (_) | (_| |  __/ |_  | |  | |  __/
|___/ \___/|_| |_|  \__| \_| |_|  \___/ \__, |\___|\__| \_|  |_/\___|
                                         __/ |                       
                                        |___/                        
              _         _
  __   ___.--'_`.     .'_`--.___   __
 ( _`.'. -   'o` )   ( 'o`   - .`.'_ )
 _\.'_'      _.-'     `-._      `_`./_
( \`. )    //\`         '/\\    ( .'/ )
 \_`-'`---'\\__,       ,__//`---'`-'_/
  \`        `-\         /-'        '/
   `                               '   
";

            Console.WriteLine(header);
        }
    }
}
