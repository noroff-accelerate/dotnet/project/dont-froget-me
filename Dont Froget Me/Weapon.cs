﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dont_Froget_Me
{
    public class Weapon
    {
        public string Category { get; set; }
        public int PowerLevel { get; set; }
        public int AmmoCapacity { get; set; }

        public Weapon(string category, int powerLevel, int ammoCapacity)
        {
            Category = category;
            PowerLevel = powerLevel;
            AmmoCapacity = ammoCapacity;
        }
    }
}
