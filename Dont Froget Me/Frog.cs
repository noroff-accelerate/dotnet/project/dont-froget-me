﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dont_Froget_Me
{
    public abstract class Frog
    {
        public string Name { get ; set ; }
        public double Weight { get ; set; }

        public void Croak()
        {
            Console.WriteLine($"Riiiibit says {Name}");
        }
        public virtual void Hop(double distanceCm)
        {
            Console.WriteLine($"{Name} hopped {distanceCm} cm away");
        }

        public abstract void Eat();

    }
}
