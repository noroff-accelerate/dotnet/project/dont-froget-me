﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dont_Froget_Me
{
    public class Degree
    {
        public string Specialization { get; set; }
        public int YearAwarded { get; set; }
        public string Institution { get; set; } 

        public Degree()
        {
            Console.WriteLine("Degree awarded");
        }
    }
}
