﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dont_Froget_Me
{
    public class MilitaryFrog : Frog, IClimb
    {
        public string Rank { get; set; }

        public Weapon Weapon { get; set; }

        public MilitaryFrog()
        {
        }

        public MilitaryFrog(string name, double weight, string rank)
        {
            Rank = rank;
        }

        public void Salute()
        {
            Console.WriteLine($"{this.Name} reporting for duty");
        }

        public void DigTrench()
        {
            Console.WriteLine("Trench is done");
        }

        public override void Hop(double distance)
        {
            Console.WriteLine($"Hopped {distance * 1.5} cm away.....HOOORAH!!");
        }

        public override void Eat()
        {
            Console.WriteLine("I ate one of my rations");
        }

        public void Climb()
        {
            Console.WriteLine("Climbing.....");
        }
    }
}
