﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dont_Froget_Me
{
    public class ScientistFrog : Frog
    {
        public string Subject { get; set; }

        public Degree Degree { get; set; }

        public ScientistFrog() : base()
        {
        }

        public ScientistFrog(string name, double weight, string subject)
        {
            Subject = subject;
        }

        public bool IsValidTestSubject(MilitaryFrog militaryFrog)
        {
            if (militaryFrog.Weight < 150)
                return true;
            else
                return false;
        }

        public void Experiment()
        {
            Console.WriteLine($"Some froglike {Subject} experiments");
        }

        public override void Eat()
        {
            Console.WriteLine("I ate on of my petri dish corn worms");
        }
    }
}